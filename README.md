## Ionic 2 App with Firebase

To use this project, either create a new ionic project using the ionic node.js utility, or copy the files from this repository into the [Starter App Base](https://github.com/ionic-team/ionic2-app-base).

### With the Ionic CLI:

```bash
$ sudo npm install -g ionic cordova
$ ionic start SwiftXe sidemenu
```

Then, to run it, cd into `SwiftXe` and run:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.

