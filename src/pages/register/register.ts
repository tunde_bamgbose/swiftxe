import { LoginPage } from './../login/login';
import { AngularFireDatabase } from 'angularfire2/database';
import { HomePage } from './../home/home';
import { EmailValidator } from './../../validators/email';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registerForm: FormGroup;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder:FormBuilder,private fbAuth:FirebaseAuthProvider,private plugins:PluginsProvider,private afDb:AngularFireDatabase) {
    this.submitAttempt = false;

    this.registerForm = this.formBuilder.group({
      fullname: ['',Validators.compose([Validators.required,Validators.pattern("^[a-zA-Z]([-']?[a-z]+)*( [a-zA-Z]([-']?[a-z]+)*)+$")])],
      email: ['',EmailValidator.isValid],
      password: ['',Validators.compose([Validators.required,Validators.minLength(6)])]
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad RegisterPage');
  }

  back(){
    this.navCtrl.pop();
  }

  signup(){
    this.submitAttempt = true;

    if(this.registerForm.valid){
      this.plugins.showLoading("Signing Up...");

      let date = new Date();
      let timestamp = date.getTime();

      let options = {
        fullname : this.registerForm.value.fullname,
        created_at: timestamp
      }

      this.fbAuth.registerWithEmail(this.registerForm.value.email,this.registerForm.value.password,options).then((data)=>{
        this.plugins.dimissLoading();
        // send the user to the homepage
        // get the user basic details and save
          this.fbAuth.getLoginStatus().then((user: any)=>{
            if(user){
              // get user details from database
              this.afDb.database.ref(`users/${ user.uid }`).once('value').then((snapshot)=>{
                // lets save in storage 
                let me = snapshot.val();
                me.email = user.email;
                this.plugins.saveInStorage("user",me);
    
                this.navCtrl.setRoot(HomePage);
              }).catch(e=>{
                this.navCtrl.setRoot(LoginPage);
              })
    
            }
            else{
              this.navCtrl.setRoot(LoginPage);
            }
          }).catch(e=>{
            this.navCtrl.setRoot(LoginPage);
          });

      }).catch((e)=>{
        console.log(e);
        this.plugins.dimissLoading();
        if(e){
          if(e.message){
            this.plugins.alert(e.message,"Unable to register");
          }
          else{
            this.plugins.alert("Please try again later","Unable to register");
          }
        }
        else{
          this.plugins.alert("Please try again later","Unable to complete");
        }
      });
    }
  }
}
