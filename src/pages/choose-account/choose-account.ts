import { PluginsProvider } from './../../providers/plugins/plugins';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-choose-account',
  templateUrl: 'choose-account.html',
})
export class ChooseAccountPage {

  accounts: any = [];
  bid:any;
  request:any;
  selected: any;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,private viewCtrl:ViewController,private fbAuth:FirebaseAuthProvider,private afDb:AngularFireDatabase,private plugins:PluginsProvider) {
    this.accounts = this.navParams.data.accounts;
    this.bid = this.navParams.data.bid;
    this.request = this.navParams.data.request;
    this.submitAttempt = false;
    // console.log(this.accounts);
    // console.log(this.bid);
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ChooseAccountPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

  changeCheck(index){
    console.log(index);
    for(var i = 0; i < (this.accounts).length; i++){
      if(index != i){
        (this.accounts)[i].checked = false;
      }
      else{
        (this.accounts)[i].checked = true;
        this.selected = (this.accounts)[i];
      }
    }
  }

  submit(){
    this.submitAttempt = true;

    if(this.selected){

      this.viewCtrl.dismiss(this.selected);
      // if(this.bid){
      //   this.afDb.database.ref(`bids/${ this.bid.request.request_id }/${ this.bid.bid_id }`).update({ account_id: this.selected.key}).then(()=>{
      //     this.submitAttempt = false;
      //     this.viewCtrl.dismiss(this.selected);
      //   }).catch((e)=>{
      //     this.submitAttempt = false;
      //     this.plugins.alert("Please try again later","Unable to complete");
      //   })
      // }
      // else{
      //   this.afDb.database.ref(`requests/${ this.request.key }`).update({ account_id: this.selected.key }).then(()=>{
      //     this.submitAttempt = false;
      //     this.viewCtrl.dismiss(this.selected);
      //   }).catch((e)=>{
      //     this.submitAttempt = false;
      //     this.plugins.alert("Please try again later","Unable to complete");
      //   })
      // }
    }
    else{
      this.submitAttempt = false;
      console.log(this.selected);
    }
  }

}
