import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChooseAccountPage } from './choose-account';

@NgModule({
  declarations: [
    ChooseAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(ChooseAccountPage),
  ],
})
export class ChooseAccountPageModule {}
