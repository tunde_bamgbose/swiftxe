import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-accept-bid',
  templateUrl: 'accept-bid.html',
})
export class AcceptBidPage {

  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,private plugins:PluginsProvider,private afDb:AngularFireDatabase,private fbAuth:FirebaseAuthProvider,private events:Events) {
    this.data = this.navParams.data.bid;
    //console.log(this.data);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AcceptBidPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

  parseNumber(number){
    return parseFloat(number);
  }

  reject(){
    
  }

  accept(){
    // put confirm here
    this.plugins.showLoading("");

    // get ref to bid
    let bidRef = this.afDb.database.ref(`bids/${ this.data.request_id }/${ this.data.bid_id }`);

    // get ref to request
    let reqRef = this.afDb.database.ref(`requests/${ this.data.request_id }`);

    // get request details
    reqRef.once("value",(snap)=>{
      let total_accepted = (snap.val()).total_accepted;
      let principal = (snap.val()).amount;
      let total_bids_accepted = (snap.val()).total_bids_accepted ? parseInt((snap.val()).total_bids_accepted) : 0;
      let amount_to_pay =  (snap.val()).amount_to_pay ? parseFloat((snap.val()).amount_to_pay ) : 0;

      // check if bid_principal is greater than amount left. if true reject
      let amount_left = parseFloat(principal) - parseFloat(total_accepted);

      if(parseFloat(this.data.bid_principal) > amount_left){
        // reject bid
        this.plugins.dimissLoading();
        this.plugins.alert("Sorry, this bid exceeds the amount left","Error");
      }
      else{
        // update bid status to accepted
        let timestamp = (new Date()).getTime();

        bidRef.update({ "status": "accepted","accepted_at":timestamp}).then(()=>{

          let sum = parseFloat(total_accepted) + parseFloat(this.data.bid_principal);

          // update bid_status in my_bids
          this.afDb.database.ref(`my_bids/${ this.data.user_id }/${ this.data.request_id }`).update({bid_status: 'accepted'});

          total_bids_accepted += 1;


          let pay = amount_to_pay + (parseFloat(this.data.bid_principal) * parseFloat(this.data.bid_rate));
          
          reqRef.update({ total_accepted : sum, total_bids_accepted: total_bids_accepted, amount_to_pay : pay });
    
          this.plugins.dimissLoading();
          this.plugins.alert("You have accepted this bid","Success");
          this.viewCtrl.dismiss(this.data.bid_principal);

          this.events.publish("bid:accepted");
      
        }).catch((e)=>{
          this.plugins.dimissLoading();
          this.plugins.alert("Please try again later","Unable to complete");
        })
      }

    }) 


    
  }

}
