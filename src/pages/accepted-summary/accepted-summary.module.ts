import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcceptedSummaryPage } from './accepted-summary';

@NgModule({
  declarations: [
    AcceptedSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(AcceptedSummaryPage),
  ],
})
export class AcceptedSummaryPageModule {}
