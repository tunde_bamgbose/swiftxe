import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-accepted-summary',
  templateUrl: 'accepted-summary.html',
})
export class AcceptedSummaryPage {
  
  data: any;
  account: any;
  loading: any;
  platform: any;
  loadingPlatform: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private plugins:PluginsProvider,private fbAuth:FirebaseAuthProvider,private afDb:AngularFireDatabase,private modalCtrl:ModalController,private events:Events) {
    this.data = this.navParams.data.data;

    console.log(this.data);
    

    if(this.data.account_id){
      this.loading = true;

      this.fbAuth.getLoginStatus().then((user:any)=>{
        let user_id = user.uid;

        this.afDb.database.ref(`my_accounts/${ user_id }/${ this.data.account_id }`).once("value",(snap)=>{
          this.account = snap.val();
          this.loading = false;
        });
      })
    }

    // get platform account for currency_symbol of request
    this.afDb.database.ref('platform_accounts').orderByChild("currency_code").equalTo(this.data.currency_code).once("value",(snap)=>{
      this.platform = this.snapshotToArray(snap);
      
    })

    //console.log(this.data);
    this.events.subscribe('account:created',()=>{
      if(this.navCtrl.getPrevious().id == 'AcceptedSummaryPage'){
        this.connectAccount();
      }
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AcceptedSummaryPage');
  }

  formatDate(timestamp){
    let dt = new Date(timestamp);
    return dt.toUTCString();
  }

  formatNumber(number){
    return parseFloat(number);
  }

  connectAccount(){
    // this will only happen if the user has not selected a default account
    // check if there user has any accounts added
    this.plugins.showLoading("");

    this.fbAuth.getLoginStatus().then((user: any)=>{

      if(user){
        let user_id = user.uid;

        // get my accounts
        this.afDb.database.ref(`my_accounts/${ user_id }`).once("value",(snap)=>{
          let accounts = this.snapshotToArray(snap);

          this.plugins.dimissLoading();

          if(accounts.length == 0){
            // send to add account page
            this.navCtrl.push('AddAccountPage');
          }
          else{
            // show select modal
            let arr=[];

            accounts.forEach(element => {
              let obj = element
              obj.checked = false;
              arr.push(obj);
            });
            let modal = this.modalCtrl.create('ChooseAccountPage',{  accounts: arr, bid : this.data})
            modal.present();

            modal.onDidDismiss((data)=>{
              if(data){
                //this.account = data;
                //this.data.account_id = data.key;
                this.plugins.showLoading('Updating...');

                this.afDb.database.ref(`bids/${ this.data.request.request_id }/${ this.data.bid_id }`).update({ account_id: data.key}).then(()=>{
                  this.plugins.dimissLoading();
                  this.account = data;
                  this.data.account_id = data.key;
                }).catch((e)=>{
                  this.plugins.dimissLoading();
                  this.plugins.alert("Please try again later","Unable to complete");
                })
              }
            }); 
          }
        }).catch((e)=>{
          this.plugins.dimissLoading();
        });
      }
      else{
        this.plugins.dimissLoading();
      }
    }).catch(()=>{
      this.plugins.dimissLoading();
    })
  }

  snapshotToArray(snapshot) {
    var returnArr = [];

    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });

    return returnArr;
  };

}
