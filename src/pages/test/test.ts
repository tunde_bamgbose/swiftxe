import { AngularFireDatabase } from 'angularfire2/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private afDb:AngularFireDatabase) {

    //this.send();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPage');
  }


  // send(){
  //   let arr = [
  //     {
  //       account_name: 'SwiftFx Naira',
  //       account_number: '0000123456',
  //       bank_name: 'Diamond Bank',
  //       location: 'Nigeria',
  //       currency_code: 'NGN'
  //     },
  //     {
  //       account_name: 'SwiftFx Dollar',
  //       account_number: '0001234567',
  //       bank_name: 'Bank of America',
  //       location: 'United States',
  //       currency_code: 'USD'
  //     },
  //     {
  //       account_name: 'SwiftFx Pounds',
  //       account_number: '0012345678',
  //       bank_name: 'Barclays',
  //       location: 'England, United Kingdom',
  //       currency_code: 'GBP'
  //     },
  //     {
  //       account_name: 'SwiftFx Euro',
  //       account_number: '0123456789',
  //       bank_name: 'Swiss Bank',
  //       location: 'Switzerland',
  //       currency_code: 'EUR'
  //     }
  //   ];

  //   arr.forEach(element => {
  //     this.afDb.database.ref('platform_accounts').push(element);
  //   });
  // }
}
