import { AngularFireDatabase } from 'angularfire2/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-view-bids',
  templateUrl: 'view-bids.html',
})
export class ViewBidsPage {

  data: any;
  bids: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private viewCtrl:ViewController,private afDb:AngularFireDatabase) {
    this.data = this.navParams.data;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ViewBidsPage');
  }

  ionViewDidEnter(){
    this.afDb.database.ref(`bids/${ this.data.request_id }`).once("value",(snap)=>{

      let result = this.snapshotToArray(snap);

      let list: any = [];


      result.forEach((item)=>{
        let obj: any = {};
        obj = item;

        this.afDb.database.ref(`users/${ item.user_id }`).once("value",(userSnap)=>{
          obj.user = userSnap.val();
        });

        list.push(obj);
      });
      
      this.bids = list;
      console.log(this.bids);
    });
  }

  close(){
    this.viewCtrl.dismiss();
  }

  snapshotToArray(snapshot) {
    var returnArr = [];

    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });

    return returnArr;
};

}
