import { AngularFireDatabase } from 'angularfire2/database';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { DatePicker } from 'ionic2-date-picker';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import 'rxjs/add/operator/map';

const numberMask = createNumberMask({
  prefix: '',
  suffix: '', // This will put the dollar sign at the end, with a space.
  allowDecimal: true,
  decimalSymbol: '.'
});

@IonicPage()
@Component({
  selector: 'page-edit-request',
  templateUrl: 'edit-request.html',
  providers: [ DatePicker ]
})
export class EditRequestPage {

  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";

  date: any;
  mask = numberMask;
  box_price: any;
  box_price_formatted: any;
  currencies: any = [];

  postForm: FormGroup;
  submitAttempt: boolean;
  loading: boolean;

  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private datePicker:DatePicker,private modalCtrl:ModalController,private viewCtrl:ViewController,private http:Http,private formBuilder:FormBuilder,private fbAuth:FirebaseAuthProvider,private plugins:PluginsProvider,private afDb:AngularFireDatabase) {
    this.datePicker = new DatePicker(<any>this.modalCtrl, <any>this.viewCtrl);
    this.datePicker.onDateSelected.subscribe((date) => { 
      this.date = date.toDateString();
    });

    this.submitAttempt = false;
    this.loading = false;
    
    this.data = this.navParams.data;
    
    this.http.get('assets/json/currency.json').map(r=>r.json()).subscribe(data=>{
      this.currencies =  data;
    });

    this.postForm = this.formBuilder.group({
      base_currency: [this.data.base_currency_code,Validators.required],
      currency: [this.data.currency_code,Validators.required],
      amount: [this.format(this.data.amount),Validators.required],
      date: [this.data.date_needed,Validators.required],
      rate: [this.format(this.data.preferred_rate),Validators.required]
    });
  }


  showCalendar(){
    this.datePicker.showCalendar();
  }

  format(valString) {
      if (!valString) {
          return '';
      }
      let val = valString.toString();
      const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
      return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);
  };

  unFormat(val) {
      if (!val) {
          return '';
      }
      val = val.replace(/^0+/, '');

      if (this.GROUP_SEPARATOR === ',') {
          return val.replace(/,/g, '');
      } else {
          return val.replace(/\./g, '');
      }
  };

  submit(){
    this.submitAttempt = true;

    if(this.postForm.valid){
      this.loading = true;
      // save into db
      // get user id
      this.fbAuth.getLoginStatus().then((user: any)=>{

        if(user){
          let user_id = user.uid;

          let selectedCurrency = this.getCurrency(this.postForm.value.currency);

          let baseCurrency = this.getCurrency(this.postForm.value.base_currency);

          let timestamp = (new Date).getTime();

          let request = {
            user_id : user_id,
            amount: this.unFormat(this.postForm.value.amount),
            currency_code: this.postForm.value.currency,
            currency_symbol: selectedCurrency.symbol,
            currency_name: selectedCurrency.name,
            base_currency_code: this.postForm.value.base_currency,
            base_currency_symbol: baseCurrency.symbol,
            base_currency_name: baseCurrency.name,
            date_needed: this.postForm.value.date,
            updated_at: timestamp,
            status: "pending",
            total_funded: 0.00,
            total_bids: 0,
            preferred_rate: this.unFormat(this.postForm.value.rate),
            total_accepted: 0
          } 



          // update request details
          this.afDb.database.ref().child(`requests/${ this.data.request_id }`).update(request).then(data=>{
            
            this.loading = false;
            this.plugins.alert("Your request has been updated","Success");

            this.navCtrl.pop();
          }).catch(err=>{
            this.loading = false;
            this.plugins.alert("Please try again later","Unable to complete");
            console.log(err);
          });

        }

      });
    }
  }


  getCurrency(code): any{
    let row = {};
    if(this.currencies){
      for(var i=0;i < this.currencies.length; i++){
        if((this.currencies)[i].code == code){
          row = (this.currencies)[i];
        }
      }
    }
    return row;
  }

}

