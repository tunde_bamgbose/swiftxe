import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditRequestPage } from './edit-request';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [
    EditRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(EditRequestPage),
    TextMaskModule
  ],
})
export class EditRequestPageModule {}
