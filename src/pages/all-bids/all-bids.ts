import { PluginsProvider } from './../../providers/plugins/plugins';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-all-bids',
  templateUrl: 'all-bids.html',
})
export class AllBidsPage {

  data:any;
  status: any = 'pending';
  accepted: any = [];
  pending: any = [];
  account: any;
  loadingAccount: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,private afDb:AngularFireDatabase,private modalCtrl : ModalController,private events:Events,private fbAuth:FirebaseAuthProvider,private plugins:PluginsProvider) {
    this.data = this.navParams.data;

    this.loadingAccount = false;

    if(this.data.account_id){
      this.getAccount();
    }

    this.events.subscribe("bid:accepted",()=>{
      this.reloadData();
    });

    this.events.subscribe('account:created',()=>{
      
      if(this.navCtrl.getPrevious().id == 'AllBidsPage'){
        this.connectAccount();
      }
    });
  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad AllBidsPage');
  }


  reloadData(){
    // create reference to requests node
    let requestsRef = this.afDb.database.ref(`requests/${ this.data.key }`);
    

    requestsRef.once("value",(snapshot)=>{
    

      let obj: any = {};
      obj = snapshot.val();
      obj.bids = obj.total_bids;
    
      this.data = obj;
    });
  }


  snapshotToArray(snapshot) {
      var returnArr = [];

      snapshot.forEach(function(childSnapshot) {
          var item = childSnapshot.val();
          item.key = childSnapshot.key;

          returnArr.push(item);
      });

      return returnArr;
  };


  viewBid(type){
    if(type == 'pending'){
      this.navCtrl.push('SingleBidPage',{
        title: 'Pending Bids',
        type: 'pending',
        request_id: this.data.key
      });
    }
    else{
      this.navCtrl.push('SingleBidPage',{
        title: 'Accepted Bids',
        type: 'accepted',
        request_id: this.data.key
      });
    }
  }

  getAccount(){
    this.fbAuth.getLoginStatus().then((user: any)=>{
      let user_id = user.uid;
      this.loadingAccount = true;

      this.afDb.database.ref(`my_accounts/${ user_id }/${ this.data.account_id }`).once("value",(snap)=>{
        this.loadingAccount = false;
        this.account = snap.val();
      }).catch(()=>{
        this.loadingAccount = false;
      })
    })
  }

  connectAccount(){
    // this will only happen if the user has not selected a default account
    // check if there user has any accounts added
    this.plugins.showLoading("");

    this.fbAuth.getLoginStatus().then((user: any)=>{

      if(user){
        let user_id = user.uid;

        // get my accounts
        this.afDb.database.ref(`my_accounts/${ user_id }`).once("value",(snap)=>{
          let accounts = this.snapshotToArray(snap);

          this.plugins.dimissLoading();

          if(accounts.length == 0){
            // send to add account page
            this.navCtrl.push('AddAccountPage');
          }
          else{
            // show select modal
            let arr=[];

            accounts.forEach(element => {
              let obj = element
              obj.checked = false;
              arr.push(obj);
            });
            let modal = this.modalCtrl.create('ChooseAccountPage',{  accounts: arr, request : this.data})
            modal.present();

            modal.onDidDismiss((data)=>{
              if(data){
                //this.account = data;
                //this.data.account_id = data.key;
                this.plugins.showLoading("Updating...");
                this.afDb.database.ref(`requests/${ this.data.key }`).update({ account_id: data.key }).then(()=>{
                  this.plugins.dimissLoading();
                  this.account = data;
                  this.data.account_id = data.key;
                }).catch((e)=>{
                  this.plugins.dimissLoading();
                  this.plugins.alert("Please try again later","Unable to complete");
                })
              }
            }); 
          }
        }).catch((e)=>{
          this.plugins.dimissLoading();
        });
      }
      else{
        this.plugins.dimissLoading();
      }
    }).catch(()=>{
      this.plugins.dimissLoading();
    })
  }


  // viewBid(index){
  //   let bid = this.pending[index];

  //   let modal = this.modalCtrl.create('AcceptBidPage',{bid : bid});
  //   modal.present();

  //   modal.onDidDismiss((data)=>{
  //     if(data){
  //       if(data){
  //         // remove from view
  //         // this.pending = (this.pending).splice(index,1);
  //         // console.log(this.pending);
  //         this.data.total_accepted = parseFloat(this.data.total_accepted) + parseFloat(data);
  //       }
  //     }
  //   });
  // }

  
    getPaid(){
      this.plugins.showLoading("Requesting Pay...");

      let timestamp = (new Date()).getTime();

      this.afDb.database.ref(`requests/${ this.data.key }`).update({ pay_requested: true, date_pay_requested: timestamp}).then(()=>{
        this.plugins.dimissLoading();
        this.data.pay_requested = true;
        this.data.date_pay_requested = timestamp;
        this.plugins.alert("Request has been sent","Successful");
      }).catch((e)=>{
        this.plugins.dimissLoading();
        this.plugins.alert("Please try again later","Unable to complete");
      })
    }

    formatDate(date){
      return (new Date(date)).toUTCString();
    }



}
