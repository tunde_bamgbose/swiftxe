import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllBidsPage } from './all-bids';

@NgModule({
  declarations: [
    AllBidsPage,
  ],
  imports: [
    IonicPageModule.forChild(AllBidsPage),
  ],
})
export class AllBidsPageModule {}
