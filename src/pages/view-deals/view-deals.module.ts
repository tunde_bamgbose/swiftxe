import { SwipeVerticalDirective } from './../../directives/swipe-vertical/swipe-vertical';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewDealsPage } from './view-deals';
import { IonDigitKeyboard } from '../../components/ion-digit-keyboard/ion-digit-keyboard.module';

@NgModule({
  declarations: [
    ViewDealsPage,
    SwipeVerticalDirective
  ],
  imports: [
    IonDigitKeyboard,
    IonicPageModule.forChild(ViewDealsPage)
  ],
})
export class ViewDealsPageModule {}
