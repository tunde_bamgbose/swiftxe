import { PluginsProvider } from './../../providers/plugins/plugins';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-view-deals',
  templateUrl: 'view-deals.html',
})
export class ViewDealsPage {

  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";

  push: any;

  // used to check if the slide is open
  opened: any;

  // data passed via navparams
  details: any;

  investment: any = "";

  amount: any = "";

  rate: any = "";

  focus: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,private viewCtrl:ViewController,private plugins:PluginsProvider,private modalCtrl:ModalController) {
    this.push = true;
    this.opened = false;
    this.details = this.navParams.data;
    console.log(this.details);
  }

  ionViewDidLoad() {
    this.opened = true;
  }

  close(){
    this.viewCtrl.dismiss();
    this.opened  = false;
  }

  removeClass(){
    setTimeout(()=>{
      this.push = false;
    },100);
  }

  format(valString) {
      if (!valString) {
          return '';
      }
      let val = valString.toString();
      const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
      
      return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);
  };

  unFormat(val) {
      if (!val) {
          return '';
      }
      val = val.replace(/^0+/, '0');

      if (this.GROUP_SEPARATOR === ',') {
          return val.replace(/,/g, '');
      } else {
          return val.replace(/\./g, '');
      }
  };


  submit(){
    if(this.investment && this.rate){
      // invalid amount specified
      if(parseFloat(this.unFormat(this.investment)) <= 0){
        this.plugins.notify("Please specify a valid amount","bottom");
        return;
      }

      // invalid rate specified
      console.log(this.unFormat(this.rate));

      if(parseFloat(this.unFormat(this.rate)) <= 0){
        this.plugins.notify("Please specify a valid rate","bottom");
        return;
      }

      // investment is greater than amount left
      if(parseFloat(this.unFormat(this.investment)) > (this.details.amount - this.details.total_accepted)){
        if((this.details.amount - this.details.total_accepted) == 0){
          this.plugins.notify("Sorry this request has reached maximum limit for funding","bottom");
        }
        else{
          this.plugins.notify("You can only fund a maximum of "+this.details.currency_symbol+" "+this.format(this.details.amount - this.details.total_accepted),"bottom");
        }
        return;
      }

      // all good.
      let obj: any = {};
      obj = this.details;
      obj.bid_principal = this.unFormat(this.investment);
      obj.rate = this.unFormat(this.rate);
      this.navCtrl.push('ConfirmBidPage',obj);
    }
    else{
      this.plugins.notify("Please specify amount and rate","bottom");
    }
  }

  viewBids(){
    if(parseInt(this.details.total_bids) <= 0){
      this.plugins.notify("No bids have been made for this request","short");
    }
    else{
      let modal = this.modalCtrl.create('ViewBidsPage',{ request_id : this.details.key });
      modal.present();
    }
  }



}
