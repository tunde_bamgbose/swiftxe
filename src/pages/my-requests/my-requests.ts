import { MyApp } from './../../app/app.component';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-my-requests',
  templateUrl: 'my-requests.html',
})
export class MyRequestsPage {

  list: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,private fbAuth:FirebaseAuthProvider,private afDb:AngularFireDatabase,private app:MyApp,private events:Events) {
    this.events.subscribe('bid:accepted',()=>{
      this.loadData();
    }) 
  }

  ionViewDidEnter(){
    this.loadData();
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad MyRequestsPage');
  }

  loadData(){
    this.fbAuth.getLoginStatus().then( (user: any)=>{
      if(user){
        let user_id = user.uid;
        // create reference to my requests node
        let myRequestsRef = this.afDb.database.ref().child(`my_requests/${ user_id }`);


        myRequestsRef.on("value",snap=>{
          // hold final result
          let result = [];

          // returns a snapshot. convert snapshot to array
          let myRequests = this.snapshotToArray(snap);


          // create reference to requests node
          let requestsRef = this.afDb.database.ref().child("requests");

          // loop to my requests array and get request details
          myRequests.forEach(item => {

            requestsRef.orderByKey().equalTo(item.request_id).once("value",snapshot=>{
              
              let res  = this.snapshotToArray(snapshot);

              let obj: any = {};
              obj = res[0];
              obj.bids = item.total_bids;
            
              // push result into array
              result.push(obj);
            });
          });

          // 
          this.list = result;
          console.log(this.list);
        });
      }
    });
  }

  snapshotToArray(snapshot) {
      var returnArr = [];

      snapshot.forEach(function(childSnapshot) {
          var item = childSnapshot.val();
          item.key = childSnapshot.key;

          returnArr.push(item);
      });

      return returnArr;
  };

  getDate(timestamp){
    let dt = new Date(timestamp);
    return dt.toDateString();
  }

  gotoNewPost(){
    this.app.nav.push('PostOfferPage');
  }

  viewRequest(request){
    this.app.nav.push('AllBidsPage',request);
  }
}
