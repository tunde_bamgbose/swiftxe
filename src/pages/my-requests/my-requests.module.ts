import { MomentModule } from 'angular2-moment';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyRequestsPage } from './my-requests';

@NgModule({
  declarations: [
    MyRequestsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyRequestsPage),
    MomentModule
  ],
})
export class MyRequestsPageModule {}
