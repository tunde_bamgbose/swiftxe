import { AngularFireDatabase } from 'angularfire2/database';
import { Dialogs } from '@ionic-native/dialogs';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, Platform, AlertController, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-edit-bid',
  templateUrl: 'edit-bid.html',
})
export class EditBidPage {

  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";

  push: any;

  // used to check if the slide is open
  opened: any;

  // data passed via navparams
  details: any;

  investment: any = "";

  amount: any = "";

  rate: any = "";

  focus: string = '';
  
  submitAttempt: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,private viewCtrl:ViewController,private plugins:PluginsProvider,private modalCtrl:ModalController,private dialog:Dialogs,private platform:Platform,private alertCtrl:AlertController,private afDb:AngularFireDatabase,private events:Events) {
    this.push = true;
    this.opened = false;
    this.details = this.navParams.data;
    this.investment = this.format(this.details.bid_principal);
    this.rate = this.format(this.details.bid_rate);
    this.submitAttempt = false;
    console.log(this.details);
  }

  ionViewDidLoad() {
    this.opened = true;
  }

  close(){
    this.viewCtrl.dismiss();
    this.opened  = false;
  }

  removeClass(){
    setTimeout(()=>{
      this.push = false;
    },100);
  }

  format(valString) {
      if (!valString) {
          return '';
      }
      let val = valString.toString();
      const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
      
      return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);
  };

  unFormat(val) {
      if (!val) {
          return '';
      }
      val = val.replace(/^0+/, '0');

      if (this.GROUP_SEPARATOR === ',') {
          return val.replace(/,/g, '');
      } else {
          return val.replace(/\./g, '');
      }
  };


  submit(){
    if(this.investment && this.rate){
      // invalid amount specified
      if(parseFloat(this.unFormat(this.investment)) <= 0){
        this.plugins.notify("Please specify a valid amount","bottom");
        return;
      }

      // invalid rate specified
      console.log(this.unFormat(this.rate));

      if(parseFloat(this.unFormat(this.rate)) <= 0){
        this.plugins.notify("Please specify a valid rate","bottom");
        return;
      }

      // investment is greater than amount left
      if(parseFloat(this.unFormat(this.investment)) > (this.details.amount - this.details.total_accepted)){
        if((this.details.amount - this.details.total_accepted) == 0){
          this.plugins.notify("Sorry this request has reached maximum limit for funding","bottom");
        }
        else{
          this.plugins.notify("You can only fund a maximum of "+this.details.currency_symbol+" "+this.format(this.details.amount - this.details.total_accepted),"bottom");
        }
        return;
      }

      // all good.
      let obj: any = {};
      obj.bid_principal = this.unFormat(this.investment);
      obj.bid_rate = this.unFormat(this.rate);
      //this.navCtrl.push('ConfirmBidPage',obj);
      let msg = (this.details.borrower.fullname).split(" ")[0]+" will pay you "+this.details.request.base_currency_symbol+" "+this.format((parseFloat(obj.bid_principal) * parseFloat(obj.bid_rate)))+" for "+this.details.request.currency_symbol+" "+this.format(obj.bid_principal)+".\nClick ok to continue";

      if(this.platform.is('cordova')){
        this.dialog.prompt(msg,"Confirm Bid edit",["OK","Cancel"]).then((buttonIndex:any)=>{
          if(buttonIndex == "1"){
            this.updateBid(obj);
          }
        });
      }
      else{
        let confirm = this.alertCtrl.create({
          title: 'Confirm Bid Edit',
          message: msg,
          buttons: [
            {
              text: 'Cancel',
              handler: () =>{
                
              }
            },
            {
              text: 'Ok',
              handler:() => {
                this.submitAttempt = true;
                this.updateBid(obj);
              }
            }
          ]
        });
        confirm.present();
      }
      
    }
    else{
      this.plugins.notify("Please specify amount and rate","bottom");
    }
  }

  viewBids(){
    if(parseInt(this.details.total_bids) <= 0){
      this.plugins.notify("No bids have been made for this request","short");
    }
    else{
      let modal = this.modalCtrl.create('ViewBidsPage',{ request_id : this.details.request.request_id });
      modal.present();
    }
  }

  updateBid(object){
    // perform some checks here

    // create ref to bid
    let bidRef = this.afDb.database.ref(`bids/${ this.details.request.request_id }/${ this.details.bid_id }`);


    console.log(this.details.request.request_id);
    console.log(this.details.bid_id);
    

    // get my initial bid 
    bidRef.once("value",(snap)=>{
      console.log(snap.val());

      let initial_bid = (snap.val()).bid_principal;

      // get the amount left to be funded
      this.afDb.database.ref(`requests/${ this.details.request.request_id }`).once("value",(snapShot)=>{
        let request = snapShot.val();

        // how much is left that can be funded
        let amount_left = parseFloat(request.amount) - parseFloat(request.total_accepted);

        // check if new bid is more than initial bid
        if(parseFloat(object.bid_principal) > parseFloat(initial_bid)){
          // user is increasing bid
          // how much is he increasing his bid by
          // let increasing_amount = parseFloat(object.bid_principal) - parseFloat(initial_bid);


          // console.log(increasing_amount);
          // console.log(amount_left);

          // if new amount is greater than amount left, he can't edit bid
          if(object.bid_principal > amount_left){
            this.plugins.alert("Sorry your bid is more than amount left for this request","Edit Failed");
            return;
          }
          else{
            // user can edit
            // update bid details
            bidRef.update(object).then(()=>{
              this.submitAttempt = false;
              this.events.publish('bid:updated');
            }).catch((e)=>{
              this.submitAttempt = false;
              this.plugins.alert("Please try again later","Unable to complete");
            });
          }
        }
        else{
          // new bid is less than or equal to inital bid
          // update

          bidRef.update(object).then(()=>{
            this.submitAttempt = false;
            this.plugins.alert("Bid was updated","Success");
            this.events.publish('bid:updated');
          }).catch((e)=>{
            this.submitAttempt = false;
            this.plugins.alert("Please try again later","Unable to complete");
          });
        }
      });
    });
    
    
  }



}

