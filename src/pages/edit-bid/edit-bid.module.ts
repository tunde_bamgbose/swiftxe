import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditBidPage } from './edit-bid';

@NgModule({
  declarations: [
    EditBidPage,
  ],
  imports: [
    IonicPageModule.forChild(EditBidPage),
  ],
})
export class EditBidPageModule {}
