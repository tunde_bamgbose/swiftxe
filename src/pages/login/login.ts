import { AngularFireDatabase } from 'angularfire2/database';
import { HomePage } from './../home/home';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { EmailValidator } from './../../validators/email';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { RegisterPage } from './../register/register';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,private plugins:PluginsProvider,private formBuilder:FormBuilder,private fbAuth: FirebaseAuthProvider,private afDb:AngularFireDatabase ) {
    this.submitAttempt = false;

    this.loginForm = this.formBuilder.group({
      email: ['',EmailValidator.isValid],
      password: ['',Validators.required]
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LoginPage');
  }

  signup(){
    this.navCtrl.push(RegisterPage);
  }

  login(){
    this.submitAttempt = true;
    if(this.loginForm.valid){
      this.plugins.showLoading("Authenticating...");
      
      // validate login credentials
      this.fbAuth.loginWithEmail(this.loginForm.value.email,this.loginForm.value.password).then((data)=>{
         // get the user basic details and save
         this.fbAuth.getLoginStatus().then((user: any)=>{
          if(user){
            // get user details from database
            this.afDb.database.ref(`users/${ user.uid }`).once('value').then((snapshot)=>{
              // lets save in storage 
              let me = snapshot.val();
              me.email = user.email;
              this.plugins.saveInStorage("user",me);
  
              this.navCtrl.setRoot(HomePage);
            }).catch(e=>{
              this.navCtrl.setRoot(LoginPage);
            })
  
          }
          else{
            this.navCtrl.setRoot(LoginPage);
          }
        }).catch(e=>{
          this.navCtrl.setRoot(LoginPage);
        });

      }).catch((err)=>{
        this.plugins.dimissLoading();
        if(err){

          if(err.message){
            this.plugins.alert(err.message,"Login Failed");
          }
          else{
            this.plugins.alert("Please try again later","Login Failed");
          }
        }
        else{
          this.plugins.alert("Please try again later","Unable to login");
        }
      });
    }
  }

}
