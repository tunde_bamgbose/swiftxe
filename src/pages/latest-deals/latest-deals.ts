import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { MyApp } from './../../app/app.component';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/switchMap';


@IonicPage()
@Component({
  selector: 'page-latest-deals',
  templateUrl: 'latest-deals.html',
})
export class LatestDealsPage {

  

  list: any = [];

  user_id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl: ModalController,private app:MyApp,private afDb: AngularFireDatabase,private fbAuth:FirebaseAuthProvider) {
    
    this.fbAuth.getLoginStatus().then((user: any)=>{
      if(user){
        this.user_id = user.uid;
        this.loadData();
      }
      else{

      }
    }).catch(()=>{

    })
    
  }

  loadData(){
    // variable will hold final result.
    

    // create reference to all requets
    let requestsRef = this.afDb.database.ref().child('requests').limitToLast(20);

    // create reference to all users
    let usersRef = this.afDb.database.ref().child('users');

    // loop through all requests
    requestsRef.on("value",snap=>{
      let finalList = [];


      let requests = this.snapshotToArray(snap);

      requests.forEach(obj=>{
        // variable obj represents a row in requests node
      
        // create a reference to a particular user using the user_id key from request row
        let user = usersRef.child(obj.user_id);

        obj["request_id"] = obj.key;
  
        // dont watch for changes. get value once
        // might want to watch for change in user details later, change once to on
        user.once("value",userSnap=>{
          let userObj = userSnap.val();
          obj["fullname"] = userObj.fullname;
          obj["location"] = userObj.location ? userObj.location: null;
          obj["profile_image"] = userObj.profile_image ? userObj.profile_image : null;
        })
  
        // push object into list
        finalList.push(obj);
      });
      
      

      
      // set list fo finalList
      this.list = finalList.reverse();
      console.log(this.list);
    })

    

  }

  snapshotToArray(snapshot) {
      var returnArr = [];

      snapshot.forEach(function(childSnapshot) {
          var item = childSnapshot.val();
          item.key = childSnapshot.key;

          returnArr.push(item);
      });

      return returnArr;
  };



  viewDetails(item){
    // let modal = this.modalCtrl.create('ViewDealsPage',item);
    // modal.present();
    this.app.nav.push('ViewDealsPage',item);
  }

  gotoNewPost(){
    this.app.nav.push('PostOfferPage');
  }

  getDate(timestamp){
    let dt = new Date(timestamp);
    return dt.toDateString();
  }

  editDetails(item){
    //console.log(item);
    this.app.nav.push('EditRequestPage',item);
  }

  test(){
    this.app.nav.push('TestPage');
  }

}
