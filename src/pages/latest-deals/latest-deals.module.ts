import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestDealsPage } from './latest-deals';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    LatestDealsPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestDealsPage),
    MomentModule
  ],
})
export class LatestDealsPageModule {}
