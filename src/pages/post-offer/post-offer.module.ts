import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostOfferPage } from './post-offer';
import { TextMaskModule } from 'angular2-text-mask';


@NgModule({
  declarations: [
    PostOfferPage,
  ],
  imports: [
    IonicPageModule.forChild(PostOfferPage),
    TextMaskModule
  ]
})
export class PostOfferPageModule {}
