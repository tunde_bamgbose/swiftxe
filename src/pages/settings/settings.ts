import { LoginPage } from './../login/login';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  user: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,private fbAuth:FirebaseAuthProvider,private plugins:PluginsProvider) {

    this.plugins.getItemFromStorage("user").then(me=>{
      this.user = JSON.parse(me);
    }).catch(e=>{
      console.log(e);
    })

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SettingsPage');
  }

  gotoPage(page){
    this.navCtrl.push(page);
  }

  logout(){
    this.plugins.showLoading("Logging out...");

    this.fbAuth.logout().then(()=>{
      this.plugins.dimissLoading();
      this.plugins.clearStorage();
      this.navCtrl.setRoot(LoginPage);
    }).catch(()=>{
      this.plugins.dimissLoading();
      this.plugins.alert("Please try again later","Unable to logout");
    })
  }

}
