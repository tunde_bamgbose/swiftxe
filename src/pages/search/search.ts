import { Component,NgZone } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  autocompleteItems;
  autocomplete;
  service: any;

  constructor(public viewCtrl: ViewController, private zone: NgZone, public navParams: NavParams) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };

    this.service = new google.maps.places.AutocompleteService();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    this.viewCtrl.dismiss(item);
  }

  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let me = this;
    this.service.getPlacePredictions({ input: this.autocomplete.query, types: ['(regions)'] }, function (predictions, status) {
      me.autocompleteItems = [];
      me.zone.run(function () {
        predictions.forEach(function (prediction) {
          me.autocompleteItems.push(prediction.description);
        });
      });
    });
  }

}
