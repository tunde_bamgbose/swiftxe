import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleBidPage } from './single-bid';

@NgModule({
  declarations: [
    SingleBidPage,
  ],
  imports: [
    IonicPageModule.forChild(SingleBidPage),
  ],
})
export class SingleBidPageModule {}
