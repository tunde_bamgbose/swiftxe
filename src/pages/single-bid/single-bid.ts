import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';

@IonicPage()
@Component({
  selector: 'page-single-bid',
  templateUrl: 'single-bid.html',
})
export class SingleBidPage {

  title: any;
  request_id: any;
  list: any;
  type:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private afDb:AngularFireDatabase,private modalCtrl : ModalController) {
    this.list = [];
    this.title = this.navParams.data.title;
    this.request_id = this.navParams.data.request_id;
    this.type = this.navParams.data.type;
    
    if(this.navParams.data.type == 'pending'){
      this.fetchPendingBids();
    }
    else{
      this.fetchAcceptedBids();
    }
  }


  fetchAcceptedBids(){
    this.afDb.database.ref(`bids/${ this.request_id }`).orderByChild("status").equalTo("accepted").on("value",(snap)=>{
      let arr = this.snapshotToArray(snap);

      let final: any = [];

      arr.forEach(element => {
        let obj = element;
        obj.bid_id = element.key;
        obj.request_id = this.request_id;

        // get details of bidder
        this.afDb.database.ref(`users/${ element.user_id }`).once("value",(userSnap)=>{
          obj.user = userSnap.val();
        });

        final.push(obj);
      });

      this.list = final;
      //console.log(this.accepted);

    });
  }

  fetchPendingBids(){
    this.afDb.database.ref(`bids/${ this.request_id }`).orderByChild("status").equalTo("pending").on("value",(snap)=>{
      let arr = this.snapshotToArray(snap);

      let final: any = [];

      arr.forEach(element => {
        let obj = element;
        obj.bid_id = element.key;
        obj.request_id = this.request_id;

        // get details of bidder
        this.afDb.database.ref(`users/${ element.user_id }`).once("value",(userSnap)=>{
          obj.user = userSnap.val();
        });

        final.push(obj);
      });

      this.list = final;
      //console.log(this.pending);
    });
  }

  snapshotToArray(snapshot) {
    var returnArr = [];

    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });

    return returnArr;
  };

  viewBid(index){
    let bid = this.list[index];

    let modal = this.modalCtrl.create('AcceptBidPage',{bid : bid});
    modal.present();

    modal.onDidDismiss((data)=>{
      if(data){
        if(data){
          // remove from view
          this.navCtrl.pop();
        }
      }
    });
  }


}
