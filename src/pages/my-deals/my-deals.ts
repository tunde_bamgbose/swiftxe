import { MyApp } from './../../app/app.component';
import { AngularFireDatabase } from 'angularfire2/database';
import { PluginsProvider } from './../../providers/plugins/plugins';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-my-deals',
  templateUrl: 'my-deals.html',
})
export class MyDealsPage {

  list: any = [];
  user_id: any;
  timer: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private fbAuth: FirebaseAuthProvider,private plugins:PluginsProvider,private afDb:AngularFireDatabase,private ngZone:NgZone,private app:MyApp,private events:Events) {
    this.events.subscribe('bid:updated',()=>{
      this.loadData();
    });
  }


 ionViewDidEnter(){
  this.loadData();
 }


 loadData(){
  this.fbAuth.getLoginStatus().then((user:any)=>{
    if(user){
      this.user_id = user.uid;

      // create reference to my bids
      this.afDb.database.ref(`my_bids/${ this.user_id }`).on("value",(snap)=>{
        let bids = this.snapshotToArray(snap);

       

        // we have some bids to work with
        if(bids.length > 0){
          // loop through each bid and get details
          let myBids = [];

          bids.forEach(item => {
            this.afDb.database.ref(`bids/${ item.key }/${ item.bid_id }`).once("value",(snap1)=>{
              let obj: any = {};
              obj = snap1.val();
              obj.bid_id = item.bid_id;
              obj.bid_status = item.bid_status;
              obj.is_funded = item.funded ? item.funded : false

              this.afDb.database.ref(`requests/${ item.key }`).once("value",(snap2)=>{
                obj.request = snap2.val();
                obj.request.request_id = item.key;

                // get user details
                this.afDb.database.ref(`users/${ obj.request.user_id }`).once("value",(snap3)=>{
                  obj.borrower = snap3.val();
                });
              })
              myBids.push(obj);
            })
          });

          this.list = myBids;
          console.log(this.list);
        }
      });
    }
  })
 }

  snapshotToArray(snapshot) {
    var returnArr = [];

    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });

    return returnArr;
  };

  withdraw(item){
    this.plugins.showLoading("Please wait");

    let bidRef = this.afDb.database.ref().child(`bids/${ item.request.request_id }/${ item.bid_id }`);

    bidRef.once("value",(snap)=>{
      let res: any = {};
      res = snap.val();

      if(res.status != "pending"){
        // cannot perform operation
        this.plugins.dimissLoading();
        this.plugins.notify("Sorry you can not perform operation on request","bottom");
      }
      else{
        bidRef.update({ status: "cancelled"}).then(()=>{

          // update bid_status in my_bids
          this.fbAuth.getLoginStatus().then((status: any)=>{
            if(status){
              let user_id = status.uid;

              this.afDb.database.ref(`my_bids/${ user_id }/${item.request.request_id }`).update({bid_status: 'cancelled'});
            }
          });

          item.status = "cancelled";
          this.plugins.dimissLoading();
          this.plugins.notify("Request has been withdrawn","short");
        }).catch(e=>{
          this.plugins.dimissLoading();
          this.plugins.alert("Please try again later","Unable to complete");
        });
      }
    });
  }

  getDate(timestamp){
    let dt = new Date(timestamp);
    return (dt.toDateString()).split(" ");
  }

  formatDate(timestamp){
    let dt = new Date(timestamp);
    return dt.toDateString();
  }

  editBid(item){
    this.app.nav.push('EditBidPage',item);
  }

  acceptedSummary(item){
    this.app.nav.push('AcceptedSummaryPage',{ data: item });
  }
}
