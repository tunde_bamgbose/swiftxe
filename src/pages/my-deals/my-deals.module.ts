import { MomentModule } from 'angular2-moment';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyDealsPage } from './my-deals';


@NgModule({
  declarations: [
    MyDealsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyDealsPage),
    MomentModule
  ],
})
export class MyDealsPageModule {}
