import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tabRoot1: any = 'LatestDealsPage';
  tabRoot2: any = 'MyDealsPage';
  tabRoot3: any = 'MyRequestsPage';

  constructor(public navCtrl: NavController) {

  }

  gotoSettings(){
    this.navCtrl.push('SettingsPage');
  }

}
