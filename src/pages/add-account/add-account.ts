import { PluginsProvider } from './../../providers/plugins/plugins';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-add-account',
  templateUrl: 'add-account.html',
})
export class AddAccountPage {

  accountForm:FormGroup;
  submitAttempt: boolean;
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private fbBuilder:FormBuilder,private modalCtrl:ModalController,private fbAuth:FirebaseAuthProvider,private afDb: AngularFireDatabase,private plugins:PluginsProvider,private events:Events) {
    this.submitAttempt = false;
    
    

    this.data =  Object.keys(this.navParams.data).length > 0 ? this.navParams.data : "";

    //console.log(this.data);

    if(this.data){
      this.accountForm = this.fbBuilder.group({
        'account_name' : [this.data.account_name,Validators.required],
        'account_number' : [this.data.account_number,Validators.required],
        'bank_name': [this.data.bank_name,Validators.required],
        'location': [this.data.location,Validators.required]
      });
    }
    else{
      this.accountForm = this.fbBuilder.group({
        'account_name' : ['',Validators.required],
        'account_number' : ['',Validators.required],
        'bank_name': ['',Validators.required],
        'location': ['',Validators.required]
      });

    }
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AddAccountPage');
  }

  search(){
    let modal = this.modalCtrl.create('SearchPage');
    modal.present();

    modal.onDidDismiss((data)=>{
      if(data){
        this.accountForm.patchValue({ 'location': data});
      }
    });
  }

  submit(){
    this.submitAttempt = true;

    if(this.accountForm.valid){
      this.fbAuth.getLoginStatus().then((user: any)=>{

        if(user){
          let user_id = user.uid;

          let obj = {
            account_name: this.accountForm.value.account_name,
            account_number: this.accountForm.value.account_number,
            bank_name: this.accountForm.value.bank_name,
            location: this.accountForm.value.location
          };

          // creating new data
          if(!this.data){
            let accountRef = this.afDb.database.ref(`my_accounts/${ user_id }`);

            accountRef.push(obj).then(()=>{
              this.submitAttempt = false;
              this.plugins.alert("Account information added","Success");              
              this.navCtrl.pop();
              this.events.publish("account:created");
            },(e)=>{
              this.plugins.alert("Please try again later","Unable to complete");
            })
          }
          else{
            // updating data
            let accountRef = this.afDb.database.ref(`my_accounts/${ user_id }/${ this.data.key }`).update(obj).then(()=>{
              this.submitAttempt = false;
              this.plugins.alert("Account information updated","Success");
              this.events.publish("account:created");
              this.navCtrl.pop();
            },(e)=>{
              this.plugins.alert("Please try again later","Unable to complete");
            });
          }

        }

      }).catch((e)=>{
        this.submitAttempt = false;
        this.plugins.alert("An unexpected error occurred. Please try again","Oops");
      })
    }
  }
}
