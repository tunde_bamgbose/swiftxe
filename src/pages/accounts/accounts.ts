import { PluginsProvider } from './../../providers/plugins/plugins';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-accounts',
  templateUrl: 'accounts.html',
})
export class AccountsPage {

  loading: boolean = false;
  accounts: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,private fbAuth:FirebaseAuthProvider,private afDb:AngularFireDatabase,private events:Events,private plugins:PluginsProvider) {
    this.loadData();
    this.events.subscribe("account:created",()=>{
      this.loadData();
    })
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AccountsPage');
  }


  addAccount(){
    this.navCtrl.push('AddAccountPage');
  }

  loadData(){
    this.loading = true;
    this.fbAuth.getLoginStatus().then((user: any)=>{
      this.loading = false;
      if(user){
        let user_id = user.uid;

        // get all my account information
        this.afDb.database.ref(`my_accounts/${ user_id }`).once("value",(snap)=>{
          let arr = this.snapshotToArray(snap);
          this.accounts = arr;
          console.log(arr);
        });
      }
    }).catch((e)=>{
      this.loading = false;  
    });
  }

  snapshotToArray(snapshot) {
    var returnArr = [];

    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });

    return returnArr;
  };

  edit(index){
    let item = this.accounts[index];
    this.navCtrl.push('AddAccountPage',item);
  }

  delete(index){
    let item = this.accounts[index];
    this.fbAuth.getLoginStatus().then((data: any)=>{
      if(data){
        let user_id = data.uid;

        this.plugins.showLoading("Deleting...");

        this.afDb.database.ref(`my_accounts/${ user_id }/${ item.key }`).remove().then(()=>{
          (this.accounts).splice(index,1);
          this.plugins.dimissLoading();
          this.plugins.notify("Account successfully removed","bottom");
        }).catch((e)=>{
          this.plugins.dimissLoading();
          this.plugins.alert("Please try again later","Unable to complete");
        })
      }
    }).catch((ex)=>{

    });
  }

}
