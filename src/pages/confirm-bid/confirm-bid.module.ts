import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmBidPage } from './confirm-bid';

@NgModule({
  declarations: [
    ConfirmBidPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmBidPage),
  ],
})
export class ConfirmBidPageModule {}
