import { PluginsProvider } from './../../providers/plugins/plugins';
import { FirebaseAuthProvider } from './../../providers/firebase-auth/firebase-auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-confirm-bid',
  templateUrl: 'confirm-bid.html',
})
export class ConfirmBidPage {

  data: any;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,private afDb:AngularFireDatabase,private fbAuth:FirebaseAuthProvider,private plugins:PluginsProvider) {
    this.data = this.navParams.data;
    console.log(this.data);
    this.submitAttempt = false;
  }

  ionViewDidLoad() {
   // console.log('ionViewDidLoad ConfirmBidPage');
  }

  parseFloat(number){
    return parseFloat(number);
  }

  submit(){
    this.fbAuth.getLoginStatus().then((user: any)=>{
        if(user){

          this.submitAttempt = true;

          let user_id = user.uid;

          let obj: any = {};
          let timestamp = (new Date()).getTime();
          obj.user_id = user_id;
          obj.bid_principal = this.data.bid_principal;
          obj.bid_rate = this.data.rate;
          obj.created_at = timestamp;
          obj.status = 'pending';
          obj.funded = false;
          obj.received_exchange = false;
          obj.date_bid_status_changed = null;
          obj.base_currency_symbol = this.data.base_currency_symbol;
          obj.base_currency_code = this.data.base_currency_code;
          obj.base_currecy_name = this.data.base_currency_name;
          obj.currency_symbol = this.data.currency_symbol;
          obj.currency_code = this.data.currency_code;
          obj.currecy_name = this.data.currency_name;

          let bidsRef = this.afDb.database.ref().child(`bids/${ this.data.key }`);

          // check if the user has placed a bid already for this advert here

            bidsRef.orderByChild("user_id").equalTo(user_id).once("value",(snap)=>{

              // convert snapshot to array
              let arr = this.snapshotToArray(snap);
              
              // user has a bid already
              if(arr.length > 0){
                this.submitAttempt = false;
                this.plugins.alert("Sorry you already submitted a bid for this request","Error");
                return;
              }
              else{
                // user doesnt have any bid
                // creating indentity key for bid id
                let row = bidsRef.push();
                
                // updating bid id with bid details
                this.afDb.database.ref().child(`bids/${ this.data.key }/${ row.key }`).set(obj).then(()=>{
    
                  // insert into mybids
                  let newObj = {
                    bid_id: row.key,
                    bid_status: "pending"
                  };
    
                  this.afDb.database.ref().child(`my_bids/${ obj.user_id }/${ this.data.key }`).set(newObj).then(()=>{

                    // increment total_bids on requests node
                    this.afDb.database.ref().child(`requests/${ this.data.key }`).once("value",(snapshot)=>{
                      //let res = this.snapshotToArray(snapshot);
                      let res: any = {};
                      res = snapshot.val();

                      let total_bids = parseInt(res.total_bids) + 1;

                      this.afDb.database.ref(`my_requests/${ this.data.user_id }/${this.data.key}`).update({total_bids: total_bids});

                      // update total bids
                      this.afDb.database.ref().child(`requests/${ this.data.key }`).update({
                        total_bids : total_bids
                      });
                    });

                    this.submitAttempt = false;
                    this.plugins.alert("Your bid has been posted","Success");
                    this.navCtrl.popToRoot();
                  }).catch((e)=>{
                    this.submitAttempt = false;
                    this.plugins.alert("Please try again later","Unable to complete");
                    console.log(e);
                  })
    
                }).catch(e=>{
                  this.submitAttempt = false;
                  this.plugins.alert("Please try again later","Unable to complete");
                  console.log(e);
                })
              }
            });
        }
    });
  }

  snapshotToArray(snapshot) {
    var returnArr = [];

    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });

    return returnArr;
  };

}
