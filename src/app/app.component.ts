import { PluginsProvider } from './../providers/plugins/plugins';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseAuthProvider } from './../providers/firebase-auth/firebase-auth';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { Observable } from 'rxjs/Observable';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private fbAuth:FirebaseAuthProvider,private afDb:AngularFireDatabase,private plugins:PluginsProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: LoginPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.fbAuth.getLoginStatus().then((user: any)=>{
        if(user){
          // get user details from database
          this.afDb.database.ref(`users/${ user.uid }`).once('value').then((snapshot)=>{
            // lets save in storage 
            let me = snapshot.val();
            me.email = user.email;
            this.plugins.saveInStorage("user",me);

            this.rootPage = HomePage;
          }).catch(e=>{
            this.rootPage = LoginPage;
          })

        }
        else{
          this.rootPage = LoginPage;
        }
      }).catch(e=>{
        this.rootPage = LoginPage;
      })
      

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
