import { AngularFireAuth } from 'angularfire2/auth';
import { RegisterPage } from './../pages/register/register';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SuperTabsModule } from 'ionic2-super-tabs';
import { DatePicker } from 'ionic2-date-picker';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { FirebaseAuthProvider } from '../providers/firebase-auth/firebase-auth';
import { PluginsProvider } from '../providers/plugins/plugins';
import { Dialogs } from '@ionic-native/dialogs';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpModule } from '@angular/http';
import { Toast } from '@ionic-native/toast';


export const firebaseConfig = {
  apiKey: "AIzaSyCK9MJqUiMNi35ymsBm8FYUGUFXgfjlwOs",
  authDomain: "fxappdemo-6a855.firebaseapp.com",
  databaseURL: "https://fxappdemo-6a855.firebaseio.com",
  projectId: "fxappdemo-6a855",
  storageBucket: "fxappdemo-6a855.appspot.com",
  messagingSenderId: "824979397353"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DatePicker,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DatePicker,
    LoginPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseAuthProvider,
    PluginsProvider,
    Dialogs,
    AngularFireDatabase,
    AngularFireAuth,
    NativeStorage,
    Toast
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
