import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable()
export class FirebaseAuthProvider {

  constructor(public fbAuth:AngularFireAuth,public fbDb:AngularFireDatabase) {
  }

  loginWithEmail(email,password){
    return this.fbAuth.auth.signInWithEmailAndPassword(email,password);
  }

  registerWithEmail(email,password,options={}){
    return this.fbAuth.auth.createUserWithEmailAndPassword(email,password).then(newUser=>{
      // user created - insert user reference into database
      console.log(newUser.uid);
      this.fbDb.database.ref('/users').child(newUser.uid).set(options);
    })
  }

  logout(){
    return this.fbAuth.auth.signOut();
  }

  getLoginStatus(){
    let promise =  new Promise((resolve,reject)=>{
      const unsubscribe = this.fbAuth.auth.onAuthStateChanged(user=> {
        // console.log(user);
        if(!user){
          reject();
        } 
        else{
          resolve(user);
        }
        unsubscribe();
      });
    });

    return promise;
    
  }

}
