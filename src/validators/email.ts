import { FormControl } from '@angular/forms';

export class EmailValidator{

    static isValid(control: FormControl): any{
        

        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(control.value == ""){
            return { "required" : true };
        }

        if(control.value != "" && !regex.test(control.value)){
            return { "incorretMailFormat" : true };
        }

        return null;
    }
}